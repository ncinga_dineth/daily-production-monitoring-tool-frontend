import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-member-event-pane',
  templateUrl: './member-event-pane.component.html',
  styleUrls: ['./member-event-pane.component.css']
})
export class MemberEventPaneComponent implements OnInit {

  @Input() line_array = [];
  selected_factory:String;

  constructor() { 

    if(localStorage.getItem('factory_id') == 'meg-sgt'){
      this.selected_factory = "sgt";
    } else if(localStorage.getItem('factory_id') == 'ant-aal-aepz'){
      this.selected_factory = "aal-aepz"
    }

  }

  ngOnInit() {

  }

}
