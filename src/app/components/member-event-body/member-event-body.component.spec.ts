import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberEventBodyComponent } from './member-event-body.component';

describe('MemberEventBodyComponent', () => {
  let component: MemberEventBodyComponent;
  let fixture: ComponentFixture<MemberEventBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberEventBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberEventBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
