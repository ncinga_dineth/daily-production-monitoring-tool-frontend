import { Component, OnInit, Input } from '@angular/core';
import { Line } from '../../classes/line'; 

@Component({
  selector: 'app-member-event-panel',
  templateUrl: './member-event-panel.component.html',
  styleUrls: ['./member-event-panel.component.css']
})
export class MemberEventPanelComponent implements OnInit {

  @Input() line_data: Line;
  @Input() btn_class: String;

  @Input() line: String;
  @Input() hsk: String;
  @Input() dis_name: String;

  constructor() { }

  ngOnInit() {
  }

}
