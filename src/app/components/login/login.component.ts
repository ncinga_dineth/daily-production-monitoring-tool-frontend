import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: String;
  password: String;

  constructor(
    private authService:AuthService,
    private router:Router
  ) { }

  ngOnInit() {
    this.authService.logout();
  }

  @HostListener('onLoginSubmit')
  onLoginSubmit(){
    const user = {
      username: this.username,
      password: this.password
    }

    this.authService.authenticateUser(user).subscribe(data => {

      if(data.token){
        this.authService.storeUserData(data.token, user.username);
        this.router.navigate(['/first_event']);
      } else if(data.success) {
        this.router.navigate(['/login']);
      }

    });
  }

}
