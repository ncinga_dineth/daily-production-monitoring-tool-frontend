import { Component, OnInit, Input } from '@angular/core';
import { Line } from '../../classes/line';

@Component({
  selector: 'app-finishing-dept',
  templateUrl: './finishing-dept.component.html',
  styleUrls: ['./finishing-dept.component.css']
})
export class FinishingDeptComponent implements OnInit {

  @Input() line_array = [];

  finishing_dept:Line[] = [
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line1",
        m_event:"red-palatte"
    },
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line2",
        m_event:"red-palatte"
    },
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line3",
        m_event:"red-palatte"
    },
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line4",
        m_event:"red-palatte"
    },
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line5",
        m_event:"red-palatte"
    },
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line6",
        m_event:"red-palatte"
    },
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line7",
        m_event:"red-palatte"
    },
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line8",
        m_event:"red-palatte"
    },
    {
        factory: "aepz",
        doc_count: 0,
        dpt: "finishing",
        module: "line9",
        m_event:"red-palatte"
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {

    for(var i=0;i<this.finishing_dept.length;i++){

      this.finishing_dept[i].m_event = "red-palatte";

    }

    if(this.line_array.length != 0){

      for(var i=0;i<this.finishing_dept.length;i++){

        for(var j=0;j<this.line_array.length;j++){
  
          if(this.line_array[j].dpt == 'finishing' && this.line_array[j].factory == this.finishing_dept[i].factory && this.line_array[j].module == this.finishing_dept[i].module){
  
            this.finishing_dept[i].m_event = "green-palatte";
  
          }
        
        }
  
      }

    }

  }

}
