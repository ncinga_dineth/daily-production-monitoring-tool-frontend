import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-factory-selection',
  templateUrl: './factory-selection.component.html',
  styleUrls: ['./factory-selection.component.css']
})
export class FactorySelectionComponent implements OnInit {

  factory_id:String;
  first_name:String;

  // factory_btn_class = {
  //   ananta: "btn btn-info active",
  //   sublime: "btn btn-default"
  // }

  constructor(
    private dataService:DataService
  ) { }

  ngOnInit() {
    this.factory_id = localStorage.getItem('factory_id');
    this.first_name = localStorage.getItem('first_name');
  }

}
