import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishingDeptComponent } from './finishing-dept.component';

describe('FinishingDeptComponent', () => {
  let component: FinishingDeptComponent;
  let fixture: ComponentFixture<FinishingDeptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishingDeptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishingDeptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
