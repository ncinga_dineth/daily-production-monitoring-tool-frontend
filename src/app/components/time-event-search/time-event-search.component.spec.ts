import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeEventSearchComponent } from './time-event-search.component';

describe('TimeEventSearchComponent', () => {
  let component: TimeEventSearchComponent;
  let fixture: ComponentFixture<TimeEventSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeEventSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeEventSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
