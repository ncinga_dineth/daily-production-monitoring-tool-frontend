import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberEventPaneComponent } from './member-event-pane.component';

describe('MemberEventPaneComponent', () => {
  let component: MemberEventPaneComponent;
  let fixture: ComponentFixture<MemberEventPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberEventPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberEventPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
