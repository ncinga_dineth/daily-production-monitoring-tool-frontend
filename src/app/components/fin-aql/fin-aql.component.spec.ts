import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinAqlComponent } from './fin-aql.component';

describe('FinAqlComponent', () => {
  let component: FinAqlComponent;
  let fixture: ComponentFixture<FinAqlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinAqlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinAqlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
