import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Station } from '../../classes/station';

@Component({
  selector: 'app-line-in',
  templateUrl: './line-in.component.html',
  styleUrls: ['./line-in.component.css']
})
export class LineInComponent implements OnInit {

  selected_date:String;
  selectedStation: Station;

  @Input() subject_array;

  line_in:Station[] = [
    {
        at:"line_in",
        sub: "floor1-sewing-section1-line1-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line1-station1",
        dis_name: "TEAM01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line_in",
        sub: "floor1-sewing-section1-line2-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line2-station1",
        dis_name: "TEAM02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line_in",
        sub: "floor1-sewing-section1-line3-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line3-station1",
        dis_name: "TEAM03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line_in",
        sub: "floor1-sewing-section1-line4-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line4-station1",
        dis_name: "TEAM04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line_in",
        sub: "floor1-sewing-section2-line5-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line5-station1",
        dis_name: "TEAM05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line_in",
        sub: "floor1-sewing-section2-line6-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line6-station1",
        dis_name: "TEAM06",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line_in",
        sub: "floor1-sewing-section2-line7-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line7-station1",
        dis_name: "TEAM07",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line_in",
        sub: "floor1-sewing-section2-line8-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line8-station1",
        dis_name: "TEAM08",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"line_in",
        sub: "floor1-sewing-section2-line9-station1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line9-station1",
        dis_name: "TEAM09",
        hsk: "primary",
        f_event:"red-palatte"
    }
  ]; 

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {

  }

  ngOnChanges() {
    
    for(var i=0;i<this.line_in.length;i++){

      this.line_in[i].f_event = "red-palatte";

    }

    if(this.subject_array.length != 0){

      for(var i=0;i<this.line_in.length;i++){

        for(var j=0;j<this.subject_array.length;j++){

          if(this.subject_array[j].at == 'line_in' && this.subject_array[j].sub == this.line_in[i].sub_ex && this.subject_array[j].hsk == this.line_in[i].hsk){

            this.line_in[i].f_event = "green-palatte";

          }
        
        }

      }

    }

  }

  onSelectStation(station: Station): void {
    this.selectedStation = station;
    this.dataService.changeStation(this.selectedStation);
  }

}
