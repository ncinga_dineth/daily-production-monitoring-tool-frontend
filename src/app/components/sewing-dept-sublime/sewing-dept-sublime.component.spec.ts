import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SewingDeptSublimeComponent } from './sewing-dept-sublime.component';

describe('SewingDeptSublimeComponent', () => {
  let component: SewingDeptSublimeComponent;
  let fixture: ComponentFixture<SewingDeptSublimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SewingDeptSublimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SewingDeptSublimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
