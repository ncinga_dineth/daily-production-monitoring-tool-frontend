import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndlineQcSublimeComponent } from './endline-qc-sublime.component';

describe('EndlineQcSublimeComponent', () => {
  let component: EndlineQcSublimeComponent;
  let fixture: ComponentFixture<EndlineQcSublimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndlineQcSublimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndlineQcSublimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
