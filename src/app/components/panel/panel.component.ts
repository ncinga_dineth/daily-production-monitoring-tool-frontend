import { Component, OnInit, Input } from '@angular/core';
import { Station } from '../../classes/station';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {

  @Input() station_data: Station;
  @Input() btn_class: String;

  @Input() line: String;
  @Input() hsk: String;
  @Input() dis_name: String;

  constructor() { }

  ngOnInit() {
  }
  
}
