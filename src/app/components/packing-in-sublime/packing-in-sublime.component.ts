import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Station } from '../../classes/station'; 

@Component({
  selector: 'app-packing-in-sublime',
  templateUrl: './packing-in-sublime.component.html',
  styleUrls: ['./packing-in-sublime.component.css']
})
export class PackingInSublimeComponent implements OnInit {

  selected_date:String;
  selectedStation: Station;

  @Input() subject_array;

  packing_in:Station[] = [
    {
        at:"packing_in",
        sub: "floor3-packing-line1-station1",
        sub_ex: "meg-sgt-floor3-packing-line1-station1",
        dis_name: "K01",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"packing_in",
        sub: "floor3-packing-line2-station1",
        sub_ex: "meg-sgt-floor3-packing-line2-station1",
        dis_name: "K02",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"packing_in",
        sub: "floor3-packing-line3-station1",
        sub_ex: "meg-sgt-floor3-packing-line3-station1",
        dis_name: "K03",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"packing_in",
        sub: "floor3-packing-line4-station1",
        sub_ex: "meg-sgt-floor3-packing-line4-station1",
        dis_name: "K04",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"packing_in",
        sub: "floor3-packing-line5-station1",
        sub_ex: "meg-sgt-floor3-packing-line5-station1",
        dis_name: "K05",
        hsk: "primary",
        f_event:"red-palatte"
    },
    {
        at:"packing_in",
        sub: "floor3-packing-line6-station1",
        sub_ex: "meg-sgt-floor3-packing-line6-station1",
        dis_name: "K06",
        hsk: "primary",
        f_event:"red-palatte"
    }
  ]; 

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {
    
  }

  ngOnChanges() {

    for(var i=0;i<this.packing_in.length;i++){

      this.packing_in[i].f_event = "red-palatte";

    }

    if(this.subject_array.length != 0){
      
      for(var i=0;i<this.packing_in.length;i++){

        for(var j=0;j<this.subject_array.length;j++){

          if(this.subject_array[j].at == 'packing_in' && this.subject_array[j].sub == this.packing_in[i].sub_ex && this.subject_array[j].hsk == this.packing_in[i].hsk){

            this.packing_in[i].f_event = "green-palatte";

          }

        }

      }

    }

  }

  onSelectStation(station: Station): void {
    this.selectedStation = station;
    this.dataService.changeStation(this.selectedStation);
  }

}
