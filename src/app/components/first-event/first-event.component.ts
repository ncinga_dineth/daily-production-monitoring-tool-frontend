import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-first-event',
  templateUrl: './first-event.component.html',
  styleUrls: ['./first-event.component.css']
})
export class FirstEventComponent implements OnInit {

  factory: String;

  constructor(
    private dataService:DataService
  ) { }

  ngOnInit() {

    this.dataService.currentFactoryValue.subscribe(value => this.factory = value);

  }

}
