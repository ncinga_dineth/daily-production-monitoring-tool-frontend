import { Component, OnInit, Input } from '@angular/core';
import { Line } from '../../classes/line';

@Component({
  selector: 'app-sewing-dept-sublime',
  templateUrl: './sewing-dept-sublime.component.html',
  styleUrls: ['./sewing-dept-sublime.component.css']
})
export class SewingDeptSublimeComponent implements OnInit {
  
  @Input() line_array = [];

  sewing_dept:Line[] = [
    {
        factory: "sgt",
        doc_count: 0,
        dpt: "sewing",
        module: "line1",
        m_event:"red-palatte"
    },
    {
        factory: "sgt",
        doc_count: 0,
        dpt: "sewing",
        module: "line2",
        m_event:"red-palatte"
    },
    {
        factory: "sgt",
        doc_count: 0,
        dpt: "sewing",
        module: "line3",
        m_event:"red-palatte"
    },
    {
        factory: "sgt",
        doc_count: 0,
        dpt: "sewing",
        module: "line4",
        m_event:"red-palatte"
    },
    {
        factory: "sgt",
        doc_count: 0,
        dpt: "sewing",
        module: "line5",
        m_event:"red-palatte"
    },
    {
        factory: "sgt",
        doc_count: 0,
        dpt: "sewing",
        module: "line6",
        m_event:"red-palatte"
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {

    for(var i=0;i<this.sewing_dept.length;i++){

      this.sewing_dept[i].m_event = "red-palatte";

    }

    if(this.line_array.length != 0){

      for(var i=0;i<this.sewing_dept.length;i++){

        for(var j=0;j<this.line_array.length;j++){
  
          if(this.line_array[j].dpt == 'sewing' && this.line_array[j].factory == this.sewing_dept[i].factory && this.line_array[j].module == this.sewing_dept[i].module){
  
            this.sewing_dept[i].m_event = "green-palatte";
            this.sewing_dept[i].doc_count = this.line_array[j].doc_count;
  
          }
        
        }
  
      }

    }

  }

}
