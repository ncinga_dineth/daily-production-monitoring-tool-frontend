import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-member-event-body',
  templateUrl: './member-event-body.component.html',
  styleUrls: ['./member-event-body.component.css']
})
export class MemberEventBodyComponent implements OnInit {

  selected_date:String;
  selected_factory:String;
  line_array = [];

  constructor(
    private dataService:DataService,
    private authService:AuthService,
    private router:Router
  ) { }

  ngOnInit() {

    if(localStorage.getItem('factory_id') == 'meg-sgt'){
      this.selected_factory = "sgt";
    } else if(localStorage.getItem('factory_id') == 'ant-aal-aepz'){
      this.selected_factory = "aal-aepz"
    } else {
      this.router.navigate(['/login']);
    }

    this.dataService.currentDateValueForMemberEvent.subscribe(date => {
      
      this.selected_date = date;
      
      this.authService.getAllMemberEvents(this.selected_factory,date).subscribe(data => {

        this.line_array = data;
        console.log(data);
  
      });
    
    });

  }

  setEventDate(date){
    this.dataService.formatDateAndChangeSource(date);
  }

}
