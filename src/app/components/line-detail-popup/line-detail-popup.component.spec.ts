import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineDetailPopupComponent } from './line-detail-popup.component';

describe('LineDetailPopupComponent', () => {
  let component: LineDetailPopupComponent;
  let fixture: ComponentFixture<LineDetailPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineDetailPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineDetailPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
