import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';
import { APPS } from '../../classes/apps';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  apps = APPS;

  constructor(
    private dataService:DataService,
    private router:Router
  ) { }

  ngOnInit() {

    if(localStorage.getItem('factory_id') == 'meg-sgt'){
      this.dataService.changeFactoryPane('sublime');
    } else if(localStorage.getItem('factory_id') == 'ant-aal-aepz'){
      this.dataService.changeFactoryPane('ananta');
    } else {
      this.router.navigate(['/login']);
    }

  }

  setEventDate(date){
    this.dataService.formatDateAndChangeSource(date);
  }

}
