import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberEventPanelComponent } from './member-event-panel.component';

describe('MemberEventPanelComponent', () => {
  let component: MemberEventPanelComponent;
  let fixture: ComponentFixture<MemberEventPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberEventPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberEventPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
