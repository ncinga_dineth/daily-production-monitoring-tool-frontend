import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SewingAqlComponent } from './sewing-aql.component';

describe('SewingAqlComponent', () => {
  let component: SewingAqlComponent;
  let fixture: ComponentFixture<SewingAqlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SewingAqlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SewingAqlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
