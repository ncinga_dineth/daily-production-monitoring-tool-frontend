import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabPaneSublimeComponent } from './tab-pane-sublime.component';

describe('TabPaneSublimeComponent', () => {
  let component: TabPaneSublimeComponent;
  let fixture: ComponentFixture<TabPaneSublimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabPaneSublimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabPaneSublimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
