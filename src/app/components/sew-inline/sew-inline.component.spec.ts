import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SewInlineComponent } from './sew-inline.component';

describe('SewInlineComponent', () => {
  let component: SewInlineComponent;
  let fixture: ComponentFixture<SewInlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SewInlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SewInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
