import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http'; 
import { map } from 'rxjs/operators';
import { JwtHelperService as jwtHelper} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authToken: any;
  user: any;

  constructor(
    private http:Http
  ) { }

  getFirstEvent(line, factory, date, hsk, at){
    return this.http.get('/monitoring-app-api/events/get_first_event?line=' + line + '&factory=' + factory + '&date=' + date + '&hsk=' + hsk + '&at=' + at).pipe(map(res => res.json()));  
  }

  getTimeEvent(index, line){
    return this.http.get('/monitoring-app-api/events/get_time_event?line=' + line + '&index=' + index).pipe(map(res => res.json()));  
  }

  getAllIndexes(){
    return this.http.get('/monitoring-app-api/events/get_all_indexes').pipe(map(res => res.json()));  
  }

  getAllSubjectsForIndex(index){
    return this.http.get('/monitoring-app-api/events/get_all_subjects_for_index?index=' + index).pipe(map(res => res.json())); 
  }

  getAllFirstEvents(factory,date){
    return this.http.get('/monitoring-app-api/events/get_all_first_events?factory=' + factory +'&date='+ date).pipe(map(res => res.json())); 
  }

  getAllMemberEvents(factory,date){
    return this.http.get('/monitoring-app-api/events/get_all_member_events?factory=' + factory +'&date='+ date).pipe(map(res => res.json())); 
  }

  authenticateUser(user){
    return this.http.get('/monitoring-app-api/users/authenticate?username=' + user.username + '&password=' + user.password).pipe(map(res => res.json()));
  }

  storeUserData(token, username){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', username);

    const helper = new jwtHelper();
    var tokenPayload = helper.decodeToken(token);
    localStorage.setItem('factory_id', tokenPayload.factory_id);
    localStorage.setItem('first_name', tokenPayload.first_name);

    this.authToken = token;
    this.user = username;
  }

  isAuthenticated(): boolean {

    if (localStorage.getItem('id_token')) {
      // logged in so return true
      return true;
    }
    return false;

  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('id_token');
    localStorage.removeItem('user');
  }

}
