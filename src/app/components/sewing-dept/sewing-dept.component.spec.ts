import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SewingDeptComponent } from './sewing-dept.component';

describe('SewingDeptComponent', () => {
  let component: SewingDeptComponent;
  let fixture: ComponentFixture<SewingDeptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SewingDeptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SewingDeptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
