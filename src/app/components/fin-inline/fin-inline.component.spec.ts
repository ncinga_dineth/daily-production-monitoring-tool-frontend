import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinInlineComponent } from './fin-inline.component';

describe('FinInlineComponent', () => {
  let component: FinInlineComponent;
  let fixture: ComponentFixture<FinInlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinInlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
